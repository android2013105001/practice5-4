package com.example.dong.practice04_1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentB extends Fragment {

    final static String ARG_POSITION = "position";
    int mCurrentPostion = -1;
    TextView def;

    public FragmentB() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (savedInstanceState != null)
        {
            mCurrentPostion = savedInstanceState.getInt(ARG_POSITION);
        }

        View fragmentView = inflater.inflate(R.layout.definition_view, container, false);
        def = (TextView) fragmentView.findViewById(R.id.definition);
        return fragmentView;
    }

    @Override
    public void onStart(){
        super.onStart();

        Bundle args = getArguments();
        if(args != null)
        {
            updateDafinitionView(args.getInt(ARG_POSITION));
        }
        else if(mCurrentPostion != -1)
        {
            updateDafinitionView(mCurrentPostion);
        }
    }

    public void updateDafinitionView(int position)
    {
        def.setText(Data.definition[position]);
        mCurrentPostion = position;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt(ARG_POSITION,mCurrentPostion);
    }




}
